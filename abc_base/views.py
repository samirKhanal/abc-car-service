from django.shortcuts import render
from abc_base.models import *

def Home(request):
    return render(request, 'base/index.html')

def AbcCarListing(request):
    abc_cars = AbcCar.objects.all()

    context = {
        'abc_cars':abc_cars
    }
    return render(request, 'base/abc_car_listing.html', context)
