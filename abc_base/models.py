from distutils.command.upload import upload
from tkinter import CASCADE
from django.db import models
from django.core.validators import *

class AbcArea(models.Model):
    street = models.CharField(max_length = 20)
    city = models.CharField(max_length = 20)
    state = models.CharField(max_length = 20)
    pincode = models.CharField(validators = [MinLengthValidator(6), MaxLengthValidator(10)],max_length = 10,unique=True)

    class Meta:
        verbose_name_plural = "AbcArea"
        db_table = 'abc_area'

class AbcCar(models.Model):
    model = models.CharField(max_length=50) 
    brand = models.CharField(max_length=50)
    category = models.CharField(max_length=50)
    color = models.CharField(max_length=10)
    image_url = models.ImageField(upload_to='images')
    capacity = models.IntegerField(blank=False, null=False)
    distance_travelled = models.IntegerField(blank=False, null=False, default=0)
    price = models.DecimalField(max_digits=18, decimal_places=2)
    description = models.TextField()
    year = models.IntegerField(blank=False, null=False)
    entry_date = models.DateField()
    is_available = models.BooleanField(default=True, null=False)
    area = models.ForeignKey(AbcArea, on_delete=models.DO_NOTHING)

    class Meta:
        verbose_name_plural = "AbcCar"
        db_table = 'abc_car'
