from unicodedata import name
from django.urls import path
from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('login/', views.login, name='login'),
    path('auth/', views.auth_client, name='auth'),
    path('logout/', views.logout, name='logout'),
    path('register/',views.register, name='register'),
    path('registration/',views.registration, name='registration'),
]