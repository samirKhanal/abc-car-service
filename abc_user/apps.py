from django.apps import AppConfig


class AbcUserConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'abc_user'
