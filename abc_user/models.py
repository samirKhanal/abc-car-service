from django.db import models
from django.core.validators import *
from django.contrib.auth.models import User

class AbcRole(models.Model):
    role_name = models.CharField(max_length=20)

    class Meta:
        verbose_name_plural = "AbcRole"
        db_table = 'abc_role'

class AbcUser(models.Model):
    first_name = models.CharField(max_length = 20)
    middle_name = models.CharField(max_length = 20)
    last_name = models.CharField(max_length = 20)
    address = models.CharField(max_length = 20)
    gender = models.CharField(max_length = 10)
    email = models.CharField(validators = [MinLengthValidator(10), MaxLengthValidator(30)], max_length = 20)
    contact_no = models.CharField(validators = [MinLengthValidator(10), MaxLengthValidator(30)], max_length = 30)
    pincode = models.CharField(validators = [MinLengthValidator(6), MaxLengthValidator(10)], max_length = 10)
    role = models.ForeignKey(AbcRole, on_delete=models.CASCADE)
    user = models.OneToOneField(User, on_delete=models.CASCADE)

    class Meta:
        verbose_name_plural = "AbcUser"
        db_table = 'abc_user_details'
    