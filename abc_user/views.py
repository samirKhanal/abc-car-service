from django.shortcuts import render, redirect
from django.contrib import auth
from django.contrib.auth import authenticate
from django.contrib.auth.models import User
from abc_user.models import *
from django.contrib.auth.decorators import login_required

def index(request):
    if not request.user.is_authenticated:
        return render(request, 'abc_user/login.html')
    else:
        return redirect('home')

def login(request):
    return render(request, 'abc_user/login.html')

def auth_client(request):
    if request.user.is_authenticated:
        return redirect('home')
    else:
        userName = request.POST['username']
        passWord = request.POST['password']
        loginUser = authenticate(request, username=userName, password=passWord)

        if loginUser is not None:
            auth.login(request, loginUser)
            return redirect('home')
        else:
            return render(request, 'abc_user/login_failed.html')

def logout(request):
    auth.logout(request)
    return redirect('home')

def register(request):
    return render(request, 'abc_user/register.html')

def registration(request):
    resultData = []
    username = request.POST['username']
    password = request.POST['password']
    firstname = request.POST['firstname']
    middlename = request.POST['middlename']
    lastname = request.POST['lastname']
    role_name = 'client'
    gender = request.POST['gender']
    contact = request.POST['contact']
    email = request.POST['email']
    address = request.POST['address']
    address = address.lower()
    pincode = request.POST['pincode']

    #user registration
    try:
        user = User.objects.create_user(username = username, password = password, email = email)
        user.first_name = firstname
        user.last_name = lastname
        user.save()
    except:
        return render(request, 'abc_user/registration_error.html')

    #user details registration
    try:
        role = AbcRole.objects.get(role_name = role_name)
    except:
        role = AbcRole(role_name=role_name)
        role.save()

    if role is None:
        return render(request, 'abc_user/registration_error.html')
    else:
        abc_user = AbcUser(user=user, first_name=firstname, middle_name=middlename, last_name=lastname, address=address, role=role, email=email, contact_no=contact, gender=gender, pincode=pincode)
        
    try:
        abc_user.save()
        return render(request, 'abc_user/registered.html')
    except:
        return render(request, 'abc_user/registration_error.html')